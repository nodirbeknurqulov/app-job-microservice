package uz.pdp.seekerservice.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.PackagePrivate;

import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity(name = "experiences")
@PackagePrivate
public class Experience {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    Boolean isCurrentJob;

    Timestamp startDate;

    Timestamp endDate;

    String jobTitle;

    @OneToOne
    Company company;

    int experienceYear;
}
