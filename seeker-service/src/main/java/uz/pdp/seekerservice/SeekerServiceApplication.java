package uz.pdp.seekerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeekerServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SeekerServiceApplication.class, args);
    }

}
