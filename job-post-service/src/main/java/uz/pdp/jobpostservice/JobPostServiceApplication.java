package uz.pdp.jobpostservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JobPostServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(JobPostServiceApplication.class, args);
    }

}
