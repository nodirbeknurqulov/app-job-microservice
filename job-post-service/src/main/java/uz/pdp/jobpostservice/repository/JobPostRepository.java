package uz.pdp.jobpostservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.jobpostservice.entity.JobPost;

public interface JobPostRepository extends JpaRepository<JobPost,Integer> {
}
