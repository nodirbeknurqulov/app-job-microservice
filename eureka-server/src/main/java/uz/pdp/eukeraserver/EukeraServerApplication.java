package uz.pdp.eukeraserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EukeraServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(EukeraServerApplication.class, args);
    }

}
