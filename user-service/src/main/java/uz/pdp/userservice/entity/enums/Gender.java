package uz.pdp.userservice.entity.enums;

public enum Gender {
    MALE, FEMALE
}
