package uz.pdp.userservice.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.PackagePrivate;
import uz.pdp.userservice.entity.enums.Gender;

import javax.persistence.*;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity(name = "users")
@PackagePrivate
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    String firstName;

    String lastName;

    String email;

    String password;

    LocalDateTime dateOfBirth;

    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    Boolean isActive;

    String contactNumber;
}
